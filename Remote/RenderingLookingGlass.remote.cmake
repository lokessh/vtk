#
# Looking Glass Remote Module
#

vtk_fetch_module(RenderingLookingGlass
        "Looking Glass Support for VTK"
        GIT_REPOSITORY https://github.com/Kitware/LookingGlassVTKModule
        GIT_TAG a970d1bee245374eec8df25a5e3d1b45b6164c6c
        )
